.. _diagnostics:

Diagnostics
===========


Tools
-----

.. automodule:: struphy.diagnostics.diagn_tools
    :members:
    :undoc-members:


.. _dispersions:

Dispersion relations 
--------------------

.. automodule:: struphy.dispersion_relations.analytic
    :members:
    :undoc-members:
    :special-members: __call__
