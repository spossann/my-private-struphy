.. _accums:

Dispersion relations
====================

Documented modules:

.. currentmodule:: ''

.. autosummary::
    :nosignatures:
    :toctree: STUBDIR

    struphy.dispersion_relations.base
    struphy.dispersion_relations.analytic

.. toctree::
    :caption: Lists of available dispersion relations:

    STUBDIR/struphy.dispersion_relations.base
    STUBDIR/struphy.dispersion_relations.analytic


Base classes
------------

.. automodule:: struphy.dispersion_relations.base
    :members:
    :undoc-members:
    :show-inheritance:


Available dispersion relations
------------------------------

.. automodule:: struphy.dispersion_relations.analytic
    :members:
    :undoc-members:
    :show-inheritance: