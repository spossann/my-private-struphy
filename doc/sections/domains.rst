.. _avail_mappings:

3D mapped domains
=================

Documented modules:

.. currentmodule:: ''

.. autosummary::
    :nosignatures:
    :toctree: STUBDIR

    struphy.geometry.base
    struphy.geometry.domains

.. toctree::
    :caption: Lists of available domains:

    STUBDIR/struphy.geometry.base
    STUBDIR/struphy.geometry.domains


Base classes
------------

.. automodule:: struphy.geometry.base
    :members:
    :show-inheritance:


Available domains
-----------------

.. automodule:: struphy.geometry.domains
    :members:
    :exclude-members: kind_map, params_map, params_numpy, F_psy, pole, periodic_eta3
    :show-inheritance:
