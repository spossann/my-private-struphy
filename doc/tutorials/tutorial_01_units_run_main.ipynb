{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1 - Run Struphy main file in a notebook\n",
    "\n",
    "In this tutorial we will learn about the Struphy main execution file `struphy/models/main/main.py`. This file is executed from the console upon calling\n",
    "```\n",
    "    $ struphy run MODEL\n",
    "```\n",
    "Please visit https://struphy.pages.mpcdf.de/struphy/sections/userguide.html for detailed information about this command. In this tutorial, we shall\n",
    "\n",
    "1. Import `struphy/models/main/main.py` and look at its functionality.\n",
    "2. Import the parameter file `params_mhd_vlasov.yml` and change some parameters.\n",
    "3. Understand the normalization of Struphy models (which units are used).\n",
    "3. Run the model [LinearMHDVlasovCC](https://struphy.pages.mpcdf.de/struphy/sections/models.html#struphy.models.hybrid.LinearMHDVlasovCC) in the notebook (without invoking the console).\n",
    "\n",
    "## Main execution file and parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from struphy.models.main import main\n",
    "\n",
    "help(main)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function `main.py` has three mandatory arguments:\n",
    "\n",
    "- `model_name`\n",
    "- `parameters`\n",
    "- `path_out`\n",
    "\n",
    "In this example, we shall simulate the current coupling hybrid model [LinearMHDVlasovCC](https://struphy.pages.mpcdf.de/struphy/sections/models.html#struphy.models.hybrid.LinearMHDVlasovCC):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model_name = 'LinearMHDVlasovCC'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " The simulation results will be stored in the Struphy installation path (obtained via `struphy -p` from the console) under the folder `io/out/tutorial_01/`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import struphy\n",
    "\n",
    "path_out = os.path.join(struphy.__path__[0], 'io/out', 'tutorial_01')"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In Struphy, parameters are passed to a model via a dictionary that is stored in `.yml` format (the \"parameter file\").\n",
    "\n",
    "Template parameter files for each model are available in the struphy installation path under the folder `io/inp/`. Let us check these out:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "inp_path = os.path.join(struphy.__path__[0], 'io/inp')\n",
    "\n",
    "os.listdir(inp_path)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The file `tutorials/params_mhd_vlasov.yml` is the one we shall use in this tutorial. \n",
    "\n",
    "Let us import it with the `yaml` package and print the obtained dictionary:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "params_path = os.path.join(inp_path, 'tutorials', 'params_mhd_vlasov.yml')\n",
    "\n",
    "import yaml\n",
    "\n",
    "with open(params_path) as file:\n",
    "    parameters = yaml.load(file, Loader=yaml.FullLoader)\n",
    "    \n",
    "parameters"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can clearly identify the 9 top-level keys mentioned in the [Struphy userguide](https://struphy.pages.mpcdf.de/struphy/sections/userguide.html#setting-simulation-parameters)."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Struphy normalization (units)\n",
    "\n",
    "Let us understand the units used in Struphy (model normalization). \n",
    "\n",
    "In the present example, the geometry is a `Cuboid` with specific left and right boundaries (and thus side length) in each of the three space directions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "parameters['geometry']"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The question arises in which units of length these numbers are expressed. From the console, the units could be checked by typing\n",
    "```\n",
    "    $ struphy units -i tutorials/params_mhd_vlasov.yml LinearMHDVlasovCC\n",
    "```\n",
    "Here, two informations are passed, namely the parameter file (`params_mhd_vlasov.yml`) and the model name (`LinearMHDVlasovCC`). \n",
    "\n",
    "The latter is obvious because each Struphy model has its own specific normalization, stated in the model's documentation (and docstring). \n",
    "\n",
    "The former, however, is not obvious (parameters influence the units?). **Indeed, Struphy provides the flexibility that the units of each model can be influenced by the user via the parameter file.**\n",
    "\n",
    "Let us check the relevant section in the dictionary:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "parameters['units']"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, the user can set\n",
    "\n",
    "1. the unit of length $\\hat x$ in meter\n",
    "2. the unit of the magnetic field strength $\\hat B$ in Tesla\n",
    "3. the unit of the number density $\\hat n$ in $10^{20}$ $m^{-3}$.\n",
    "\n",
    "In the above example we have $\\hat x \\approx 0.023\\,m$, $\\hat B = 1\\,T$ and $\\hat n = 10^{20}$ $m^{-3}$. "
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " All other units, such as for velocity $\\hat v$ or time $\\hat t$ etc., are derived from the three basic units above. How is this achieved? In Struphy, each model has two built-in class methods:\n",
    " \n",
    "- `velocity_scale`\n",
    "- `bulk_species`\n",
    "\n",
    "These have been set by the model developer (hard-coded) and cannot be changed by the user. They determine the derived units in the following way:\n",
    "\n",
    "The `bulk_species` sets the mass number ($A$) and charge number ($Z$) to be used in the calculation of units:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from struphy.models.hybrid import LinearMHDVlasovCC\n",
    "\n",
    "print(LinearMHDVlasovCC.bulk_species())\n",
    "\n",
    "parameters['fluid'][LinearMHDVlasovCC.bulk_species()]['phys_params']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "The `velocity_scale` (partly) determines the velocity unit $\\hat v$. It has been set by the model developer to one of the following:\n",
    "\n",
    "1. speed of light, $\\hat v = c$\n",
    "2. Alfvén speed of the bulk species, $\\hat v = v_\\textnormal{A, bulk} = \\sqrt{\\hat B^2 / (m_\\textnormal{bulk} \\hat n \\mu_0)}$\n",
    "3. Cyclotron speed of the bulk species, $\\hat v = \\hat x \\Omega_\\textnormal{c, bulk}/(2\\pi) = \\hat x\\, q_\\textnormal{bulk} \\hat B /(m_\\textnormal{bulk}2\\pi)$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(LinearMHDVlasovCC.velocity_scale())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "The three possible velocities scales are entirely defined in terms of:\n",
    "\n",
    "- the three units $\\hat x$, $\\hat B$, $\\hat n$, which are provided by the user (who can thus also influence $\\hat v$)\n",
    "- the `bulk_species` (through $m_\\textnormal{bulk} = m_\\textnormal{proton} A$ and $q_\\textnormal{bulk} = q_\\textnormal{e}Z$). \n",
    "\n",
    "The associated time scale is then automatically given by\n",
    "$$\n",
    " \\hat t = \\hat x / \\hat v \\,.\n",
    "$$\n",
    "\n",
    " To summarize: qualitatively, the `velocity_scale` and the `bulk_species` are fixed within each model by the developer (hard-coded). Quantitatively, the values (here for the Alfvén speed and the MHD charge and mass) are set by the user through the parameter file. "
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Please check out https://struphy.pages.mpcdf.de/struphy/sections/models.html#normalization for further discussion on the units used in Struphy. In this tutorial, instead of the console, we can inspect the units of our run also directly in this notebook:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "units, eq_params = LinearMHDVlasovCC.model_units(parameters, verbose=True)\n",
    "units"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Aside from the units, there are also the equation parameters `eq_params` returned, namely\n",
    "\n",
    "- `alpha_unit` being the ratio of the unit plasma frequency to the unit cyclotron frequency\n",
    "- `epsilon_unit` being the ratio of the unit angular frequency to the unit cyclotron frequency\n",
    "\n",
    "for each species."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "eq_params"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The side lengths of the `Cuboid` in our example are:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('s1 = ', (parameters['geometry']['Cuboid']['r1'] - parameters['geometry']['Cuboid']['l1']) * units['x'], 'm')\n",
    "print('s2 = ', (parameters['geometry']['Cuboid']['r2'] - parameters['geometry']['Cuboid']['l2']) * units['x'], 'm')\n",
    "print('s3 = ', (parameters['geometry']['Cuboid']['r3'] - parameters['geometry']['Cuboid']['l3']) * units['x'], 'm')"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Run Struphy main\n",
    "\n",
    "Let us get back to the parameter file and change some entries in the parameter dictionary before we run the model.\n",
    "\n",
    "The end time of 50 is too long for our example and we wish to simulate more particles-per-cell than 200 to have a higher resolution. Let us change these two parameters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "parameters['time']['Tend'] = 1.5\n",
    "parameters['kinetic']['energetic_ions']['markers']['ppc'] = 400"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We are now ready to call the Struphy main file. A tutorial of how to post-process the generated simulation data is available [here](https://struphy.pages.mpcdf.de/struphy/doc/_build/html/tutorials/tutorial_02_postproc_standard_plotting.html). \n",
    "\n",
    "The console equivalent of the following command is\n",
    "```\n",
    "    $ struphy run LinearMHDVlasovCC -i tutorials/params_mhd_vlasov.yml -o tutorial_01/\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "main(model_name, parameters, path_out)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "env",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
