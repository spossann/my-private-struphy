# Struphy - Structure-preserving hybrid codes

A Python package for plasma physics PDEs.

See the [Struphy documentation](https://struphy.pages.mpcdf.de/struphy/index.html) for details.

## Reference paper

* S. Possanner, F. Holderied, Y. Li, B.-K. Na, D. Bell, S. Hadjout and Y. Güçlü, [**High-Order Structure-Preserving Algorithms for Plasma Hybrid Models**](https://link.springer.com/chapter/10.1007/978-3-031-38299-4_28), International Conference on Geometric Science of Information 2023, 263-271, Springer Nature Switzerland.

## Contact

* Stefan Possanner [stefan.possanner@ipp.mpg.de](spossann@ipp.mpg.de)
* Eric Sonnendrücker [eric.sonnendruecker@ipp.mpg.de](eric.sonnendruecker@ipp.mpg.de)
* Xin Wang [xin.wang@ipp.mpg.de](xin.wang@ipp.mpg.de)