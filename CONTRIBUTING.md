# Repository

Struphy has two main branches, **master** and **devel**. 
Nobody can push directly to these branches.

The **master** branch holds the current release of the code. 

**devel** is the main branch for developers. Feature branches must be checked out and merged into **devel**.

# Open development

When adding code to struphy it is important that other developers can follow your plans. For this we use the [Issue tracker](https://gitlab.mpcdf.mpg.de/struphy/struphy/-/issues).
There, you can add a short description of your plans and choose one of the following ``labels``:

* Bug
* Discussion
* Documentation
* Feature Request
* ToDo

# Forking

In case you are not a [member](https://gitlab.mpcdf.mpg.de/struphy/struphy/-/project_members>) of the Struphy project,
you can contribute code by [forking](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html>)the Struphy
repository.

You must create a **public fork** to be able to merge your code into Struphy!

You can create feature branches in your forked repo and create merge requests into the original Struphy repo.

[Update your fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#from-the-command-line)
in case **devel** changes in the Struphy repo while you are working on your feature.

# More info

See the [Developer's guide](https://struphy.pages.mpcdf.de/struphy/sections/developers.html).