grid :
    Nel      : [64, 36, 3] # number of grid cells, >p
    p        : [3, 3, 3]  # spline degree, >1
    spl_kind : [False, True, True] # spline type: True=periodic, False=clamped
    bc       : [[null, null], [null, null], [null, null]] # boundary conditions for N-splines (homogeneous Dirichlet='d')
    dims_mask : [True, True, True] # True if the dimension is to be used in the mpi domain decomposition (=default for each dimension).
    nq_el    : [6, 6, 6] # quadrature points per grid cell
    nq_pr    : [6, 6, 6] # quadrature points per histopolation cell (for commuting projectors)
    polar_ck : -1 # C^k smoothness at polar singularity at eta_1=0 (default: -1 --> standard tensor product, 1 : polar splines)

units : # units not stated here can be viewed via "struphy units -h"
    x : 1. # length scale unit in m
    B : 1. # magnetic field unit in T
    n : 0.2 # number density unit in 10^20 m^(-3)

time :
    dt         : 0.1  # time step
    Tend       : 1000.0 # simulation time interval is [0, Tend]
    split_algo : LieTrotter # LieTrotter | Strang

geometry : 
    type : HollowTorusStraightFieldLine # mapping F (possible types seen below)  
    HollowTorusStraightFieldLine :
        a1       : 0.1 # inner radius
        a2       : 1.0 # minor radius
        R0       : 5.0 # major radius
        tor_period : 2 # toroidal periodicity built into the mapping: phi = 2*pi * eta3 / tor_period
            
mhd_equilibrium : 
    type : AdhocTorus # (possible choices seen below)
    AdhocTorus :
        a      : 1. # minor radius
        R0     : 5. # major radius
        B0     : 5. # on-axis toroidal magnetic field
        q0     : 1.15 # safety factor at r=0
        q1     : 1.60 # safety factor at r=a
        n1     : 0. # shape factor for number density profile 
        n2     : 0. # shape factor for number density profile 
        na     : 1. # number density at r=a
        p_kind : 1 # kind of pressure profile (0 : cylindrical limit, 1 : ad hoc)
        p1     : 0. # shape factor for ad hoc pressure profile
        p2     : 0. # shape factor for ad hoc pressure profile
        beta   : .02 # plasma beta = 2*p*mu_0/B^2

kinetic :
    ions :
        phys_params:
            A : 1  # mass number in units of proton mass
            Z : 1 # signed charge number in units of elementary charge
        markers :
            type    : full_f # full_f, control_variate, or delta_f
            ppc     : 100
            Np      : 1 # alternative if ppc = null (total number of markers, must be larger or equal than # MPI processes)
            eps     : 2.0 # MPI send/receive buffer (0.1 <= eps <= 1.0)
            bc : 
                type    : [remove, periodic, periodic] # marker boundary conditions: remove, reflect or periodic
                remove  :
                    save              : True
                    boundary_transfer : True            
            loading :
                type    : pseudo_random # particle loading mechanism 
                seed    : 1608 # seed for random number generator
                moments : [0., 0., 1., 1.] # moments of Gaussian s3, see background/moms_spec
                spatial : uniform # uniform or disc
                # initial : [[.501, 0.001, 0.001,  1.935 , -1.72], # counter-passing particle
                #            [.501, 0.001, 0.001, -1.935 , -1.72], # co-passing particle
                #            [.501, 0.001, 0.001,  0.4515, -1.72], # counter-trapped particle
                #            [.501, 0.001, 0.001, -0.6665, -1.72]] # co-trapped particle

        init : 
            type : Maxwellian5DUniform
            Maxwellian5DUniform:
                n : 0.05
        save_data :
            n_markers : 4 # number of markers to be save during simulation
        push_algos1 :
            integrator : implicit # explicit or implicit
            method : discrete_gradients # possible choices: discrete_gradients, discrete_gradients_faster, discrete_gradients_Itoh_Newton, forward_euler, heun2, rk2, heun3, rk4
            maxiter : 10
            tol : 1.e-10
        push_algos2 :
            integrator : implicit # explicit or implicit
            method : discrete_gradients_Itoh_Newton # possible choices: discrete_gradients, discrete_gradients_faster, discrete_gradients_Itoh_Newton, forward_euler, heun2, rk2, heun3, rk4
            maxiter : 10
            tol : 1.e-10
