grid :
    Nel      : [64, 2, 2] # number of grid cells, >p
    p        : [3, 1, 1]  # spline degree
    spl_kind : [True, True, True] # spline type: True=periodic, False=clamped
    bc       : [[null, null], [null, null], [null, null]] # boundary conditions for N-splines (homogeneous Dirichlet='d')
    dims_mask : [True, True, True] # True if the dimension is to be used in the mpi domain decomposition (=default for each dimension).
    nq_el    : [2, 2, 2] # quadrature points per grid cell
    nq_pr    : [2, 2, 2] # quadrature points per histopolation cell (for commuting projectors)
    polar_ck : -1 # C^k smoothness at polar singularity at eta_1=0 (default: -1 --> standard tensor product, 1 : polar splines)

units :
    x   : 0.0017045090240267625 # c * m_e / e
    B   : 1. # magnetic field unit in T
    n   : 0.09719853837468743 # eps_0 / m_e / 1e20 # bulk number density unit in 1 x 10^20 m^(-3)

time :
    dt         : 0.01 # time step
    Tend       : 0.1 # simulation time interval is [0, Tend]
    split_algo : LieTrotter # LieTrotter | Strang

geometry :
    type : Cuboid # mapping F (possible types seen below)
    Cuboid :
        l1       : 0. # start of interval in eta1
        r1       : 1. # end of interval in eta1, r1>l1
        l2       : 0. # start of interval in eta2
        r2       : 1. # end of interval in eta2, r2>l2
        l3       : 0. # start of interval in eta3
        r3       : 1. # end of interval in eta3, r3>l3

mhd_equilibrium :
    type : HomogenSlab # (possible choices seen below)
    HomogenSlab :
        B0x  : 0.5 # magnetic field in x
        B0y  : 0. # magnetic field in y
        B0z  : 1.4 # magnetic field in z
        beta : 0. # plasma beta = 2*p*mu_0/B^2
        n0   : 1. # number density

electric_equilibrium :
    type : HomogenSlab # (possible choices seen below)
    HomogenSlab :
        phi0  : 1. # constant electric potential

em_fields :
    init :
        type : ModesSin
        ModesSin : 
            coords : 'logical' # in which coordinates (logical or physical)
            comps :
                e_field : [False, False, False]  # components to be initialized
                b_field : [False, False, True] # components to be initialized
            ls : [1] # Integer mode numbers in x or eta_1 (depending on coords)
            ms : [0] # Integer mode numbers in y or eta_2 (depending on coords)
            ns : [0] # Integer mode numbers in z or eta_3 (depending on coords)
            amps : [0.01] # amplitudes of each mode

kinetic :
    electrons :
        phys_params:
            A : 0.0005446170214876324  # electron mass number in units of proton mass
            Z : -1 # signed charge number in units of elementary charge
        markers :
            type    : delta_f # full_f, control_variate, or delta_f
            Np      : 10000 # alternative if ppc not given (total number of markers, Np must be >= # MPI processes)
            eps     : 0.25 # MPI send/receive buffer (0.1 <= eps <= 1.0)
            bc : 
                type    : [periodic, periodic, periodic] # marker boundary conditions: remove, reflect or periodic
            loading :
                type          : pseudo_random # particle loading mechanism
                seed          : 1234 # seed for random number generator
                dir_particles : path_to_particles # directory of particles if loaded externally
                moments       : [0., 0., 0., 0.1, 0.1, 0.1] # moments of Gaussian s3, see background/moms_spec
                spatial       : uniform # uniform or disc
        init :
            type : Maxwellian6DPerturbed
            Maxwellian6DPerturbed :
                n :
                    n0 : 0.
                u1 :
                    u01 : 0.
                u2 :
                    u02 : 0.
                u3 :
                    u03 : 0.
                vth1 :
                    vth01 : 0.5
                vth2 :
                    vth02 : 0.5
                vth3 :
                    vth03 : 0.5
        background :
            type : Maxwellian6DUniform
            Maxwellian6DUniform :
                n  : 1.
                u1 : 0.
                u2 : 0.
                u3 : 0.
                vth1 : 0.5
                vth2 : 0.5
                vth3 : 0.5
        save_data :
            n_markers : 10 # number of markers to be saved during simulation
            f :
                slices : [e3, v1_v2] # in which directions to bin (e.g. [e1_e2, v1_v2_v3])
                n_bins : [[64], [64, 64]] # number of bins in each direction (e.g. [[16, 20], [16, 18, 22]])
                ranges : [[[0., 1.]], [[-5., 5.], [-5., 5.]]] # bin range in each direction (e.g. [[[0., 1.], [0., 1.]], [[-3., 3.], [-4., 4.], [-5., 5.]]])
        push_algos :
            vxb : analytic # possible choices: analytic, implicit
            eta : rk4 # possible choices: forward_euler, heun2, rk2, heun3, rk4

solvers :
    solver_poisson :
        type : PConjugateGradient
        pc : MassMatrixPreconditioner # MassMatrixPreconditioner # null or name of preconditioner class
        tol : 1.e-10
        maxiter : 3000
        info : False
        verbose : False
    solver_ew :
        type : PConjugateGradient
        pc : MassMatrixPreconditioner # null or name of preconditioner class
        tol : 1.e-10
        maxiter : 3000
        info : False
        verbose : False
    solver_eb :
        type : PConjugateGradient
        pc : MassMatrixPreconditioner # null or name of preconditioner class
        tol : 1.e-10
        maxiter : 3000
        info : False
        verbose : False
